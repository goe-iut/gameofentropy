<?php session_start();?>
<?php ob_start();?>


<?php
	function confirm()
	{
?>
	<div class="confirm">
		<h3>Vous confirmez le déplacement ?</h3>
		<p>
			<a class="btn btn-primary" href="?<?=$_SERVER['QUERY_STRING']?>&confirm=true"> Oui </a>
			<a class="btn btn-primary" href="?<?=$_SERVER['QUERY_STRING']?>&confirm=false"> Non </a>
		</p>
	</div>';
<?php
	}

	function creationPartie()
	{
	//Création de la partie si elle n'existe pas
		if(!isset($_SESSION['partie']) || empty($_SESSION['partie']))
		{
			$_SESSION['plateau'] = serialize(new Plateau());
			$_SESSION['partie'] = true;
		 	$_SESSION['J1'] = serialize(new Joueur('J1','Joueur 1'));
		 	$_SESSION['J2'] = serialize(new Joueur('J2','Joueur 2'));
		 	$_SESSION['debug'] = 0;
	//le joueur qui commence est défini aléatoirement
			if(rand(1,2) == 1)
				$_SESSION['isTurnOf'] = $_SESSION['J1'];
			else
				$_SESSION['isTurnOf'] = $_SESSION['J2'];
		}
	}

	function alertMSG()
	{
?>
		<h1 class="message label label-danger">NE PAS MODIFER LES VALEURS DANS L'URL</h1>
<?php
	}
?>

<!DOCTYPE html>
<html>
<head>


	<link rel="stylesheet" href="css/bootstrap.css" charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/style.css">

	<title>G-O-E</title>
	<?php
		header('Content-Type: text/html; charset=UTF-8');
		include 'pion.php';
		include 'plateau.php';
		include 'joueur.php';
	?>
</head>
<body>
	<div class="container">
		<div class="row">
	<?php
	//Permet l'affichage ou non du debug
		if(isset($_GET['debug']))
		{
			if($_GET['debug'] == 1)
				$_SESSION['debug'] = 0;
			else
				$_SESSION['debug'] = 1;
		}
		if(isset($_SESSION['debug']))
			if(!empty($_SESSION['debug']))
				$debugStatus = 'Désactiver le debug';
			else
				$debugStatus = 'Activer le debug';
		else
		{
			$_SESSION['debug'] = 1;
			$debugStatus = 'Activer le debug';
		}

		//Voir la fonction creationPartie() Tout en haut
		creationPartie();

		$plateau = unserialize($_SESSION['plateau']);
		$cases = $plateau->getCases();
	//Récupération du joueur qui doit jouer
		$isTurnOf = unserialize($_SESSION['isTurnOf']);

	//Affichage des déplacements possible si un pion est sélectionné
		if (isset($_GET['selectPion']))
		{
			$x = substr($_GET['selectPion'],0,1);
			$y = substr($_GET['selectPion'],1,1);
	//Anti triche sur l'url longue
			if($x > 5 || $x < 1 || $y > 5 || $y < 1)
			{
				alertMSG();
			}
			else
				if(!($plateau->deplacementImpossiblePion($x,$y, $isTurnOf)))
					$deplacement = $plateau->deplacementPossiblePion($x,$y,$isTurnOf);
		}
	?>

	<?php
	//Change de joueur pour le prochain tour
		if($plateau->passeTour($isTurnOf))
		{
			if($isTurnOf->getId() == 'J1')
				$_SESSION['isTurnOf'] = $_SESSION['J2'];
			else
				$_SESSION['isTurnOf'] = $_SESSION['J1'];
	//Rafraichissemnt de la page pour prendre effet du changement de tour
			header("Refresh:0; url=index.php");
		}
		else
		{
			if(isset($_GET['deplacerVers']) && isset($_GET['Pion']))
			{
				if(!isset($_GET['confirm']))
				{
					confirm();
	?>


	<?php			}
				else
					if($_GET['confirm'] == 'true')
					{
						$xPion = substr($_GET['Pion'],0,1);
						$yPion = substr($_GET['Pion'],1,1);
						$xDep  = substr($_GET['deplacerVers'],0,1);
						$yDep  = substr($_GET['deplacerVers'],1,1);
						if(!in_array($xDep.$yDep,$plateau->deplacementPossiblePion($xPion,$yPion,$isTurnOf)))
							alertMSG();
						else
						{
							$plateau->deplacementPion($xPion,$yPion,$xDep,$yDep,$isTurnOf);
							if(! $plateau->partieGagnee($isTurnOf))
							{
								if($isTurnOf->getId() == 'J1')
									$_SESSION['isTurnOf'] = $_SESSION['J2'];
								else
									$_SESSION['isTurnOf'] = $_SESSION['J1'];
					//Rafraichissemnt de la page pour prendre effet du changement de tour
								header("Refresh:0; url=?");
							}
						}
					}
					else {
						header("Refresh:0; url=?");
					}
			}
		}

		if(isset($_SESSION['debug']) && $_SESSION['debug'] == 1)
		{
			$debug = 6;
			$hidden = ' ';
		}
		else
		{
			$debug = 12;
			$hidden = 'hidden';
		}
	?>
			<div class="page-header">
				<h1>Game Of Entropy
					<a class="btn btn-warning" href="?debug=<?=$_SESSION['debug']?>"><?=$debugStatus?></a>
					 | C'est à <?=$isTurnOf?> <img src="images/<?=$isTurnOf->getId()?>.png" alt="pion <?=$isTurnOf->getId()?>"> de jouer	</h1>
			</div>
			<div class="col-md-<?=$debug?>">
				<table id="plateau">
					<tr>
	<?php
		for ($y=1; $y <= 5; $y++)
		{
			for ($x=1; $x <= 5; $x++)
			{
	?>
						<td>
	<?php
				if(!isset($deplacement))
					if(preg_match('#'.$isTurnOf->getId().'#',$cases[$x.$y]->getJoueur()))
					{
	?>
							<a href="?selectPion=<?=$x.$y?>"><?=$cases[$x.$y]?></a>
	<?php
					}
					else
					{
						echo $cases[$x.$y];
					}
				else
				{
					if(in_array(($x.$y),$deplacement))
					{
	?>
							<a href="?Pion=<?=$_GET['selectPion']?>&deplacerVers=<?=($x.$y)?>">
								<div class="deplace"><?=$cases[$x.$y]?></div>
							</a>
	<?php
					}
					else
					{
						if(preg_match('#'.$isTurnOf->getId().'#',$cases[$x.$y]->getJoueur()))
						{
	?>
							<a href="?selectPion=<?=$x.$y?>"><?=$cases[$x.$y]?></a>
	<?php
						}
						else
						{
							echo $cases[$x.$y];
						}
					}
				}
	?>
						</td>
	<?php
				if($x == 5)
				{
	?>
					</tr><tr>
	<?php
				}
			}
		}
	?>
					</tr>
				</table>
	<?php
		if($plateau->partieGagnee($isTurnOf))
		{
	?>
				<h1 class="alert alert-info">Le <?=$isTurnOf?> <img src="images/<?=$isTurnOf->getId()?>.png" alt="pion <?=$isTurnOf->getId()?>"> à Gagné !!!</h1>
	<?php
		}
	 ?>
			</div>

			<div class="<?=$hidden?> col-md-6">
				<pre>
					<h2> Debug </h2>
						<p>COOKIE		<p><?=var_dump($_COOKIE)?></p></p>
						<p>GET 			<p><?=var_dump($_GET)?></p></p>
						<p>SESSION 		<p><?=var_dump($_SESSION);?></p></p>
				</pre>
			</div>
	<?php
		//Sauvegarde du plateau pour la prochain chargement
		$_SESSION['plateau'] = serialize($plateau);
	 ?>
 		</div>
 	</div>
</body>
</html>
