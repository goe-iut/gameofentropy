<?php
	/**
	*
	*/
	class Joueur
	{

		function __construct($id,$nom)
		{
			$this->id  = $id;
			$this->nom = $nom;
		}

		function getId()
		{
				return $this->id;
		}

		function getNom()
		{
			return $this->nom;
		}

		function __toString(){
			return $this->nom;
		}
	}
?>
