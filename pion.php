<?php

/**
*
*/
class Pion
{
	private $x;
	private $y;
	private $joueur;

	function __construct($joueur, $x, $y)
	{
		$this->x = $x;
		$this->y = $y;
		$this->joueur = $joueur;
		$this->seul = false;
	}

	function getX()
	{
		return $this->x;
	}

	function setX($coordX)
	{
		$this->x = $coordX;
	}

	function getY()
	{
		return $this->y;
	}

	function setY($coordY)
	{
		$this->y = $coordY;
	}

	function getJoueur()
	{
		return $this->joueur;
	}

	function __toString()
	{
		if($this->joueur == NULL)
			return '<img src="images/null.png">';
		if(preg_match('#J1#',$this->joueur))
			return '<img src="images/J1.png">';
		elseif (preg_match('#J2#',$this->joueur))
			return '<img src="images/J2.png">';
	}
}
