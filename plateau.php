<?php
	/**
	@Class Plateau

	*/
	class Plateau
	{

		private $cases = array();

		function __construct()
		{
			for ($y=1; $y <= 5; $y++) { //pour chaque ligne du plateau
				for ($x=1; $x <= 5; $x++) { //pour chaque colonne du plateau
					if($y == 1)
						$this->cases[$x.$y]=new Pion('J1', $x, $y); //init des pions du joueur 1 sur la premières ligne
					elseif($y == 2 && ($x == 1 || $x == 5))
						$this->cases[$x.$y]=new Pion('J1', $x, $y); //init des deux pions du joueur 1 sur la seconde ligne
					elseif($y == 5)
						$this->cases[$x.$y]=new Pion('J2', $x, $y); //init des pions du joueur 2 sur la dernière ligne
					elseif($y == 4 && ($x == 1 || $x == 5))
						$this->cases[$x.$y]=new Pion('J2', $x, $y); //init des deux pions du joueur 2 sur l'avant dernière ligne
					else
						$this->cases[$x.$y]=new Pion(NULL, $x, $y); //init des pions ayant pour joueur NULL dans tous les autres emplacements
				}
			}
		}

		//renvoit les caractéristiques du plateau (cases étant une matrice contenant tout les pions du plateau)
		function getCases()
		{
			return $this->cases;
		}

		/**
		@function deplacementPossiblePion
		@param coordX int Coordonnée de colonne, coordY int Coordonnée de ligne, joueur Joueur Le joueur courant

		@return Matrice de pion

		descritpion
		fonction qui retourne la liste des déplacements possible du pion courant. Prend en compte les reconnexions si besoin.
		*/
		function deplacementPossiblePion($coordX, $coordY, $joueur)
		{

			$caseDispo = array(); //création de la liste de case disponible pour déplacement
			$casesDispos = array();
			$x = $coordX; //variable qui changera lors du décalage sur l'axe horizontale
			$y = $coordY; //de même mais pour l'axe verticale

			//on commence par l'axe horizontale, sens : pion -> gauche
			$x--;
			while(isset($this->cases[$x.$y]) && $this->cases[$x.$y]->getJoueur() == NULL)
			{
				$caseDispo[] = $x.$y;
				$x--;
			}
			$x = $coordX;

			//puis diagonale, sens : pion -> gauche,bas
			$x--;
			$y++;
			while(isset($this->cases[$x.$y]) && $this->cases[$x.$y]->getJoueur() == NULL)
			{
				$caseDispo[] = $x.$y;
				$x--;
				$y++;
			}
			$x = $coordX;
			$y = $coordY;

			//axe verticale, sens : pion -> bas
			$y++;
			while(isset($this->cases[$x.$y]) && $this->cases[$x.$y]->getJoueur() == NULL)
			{
				$caseDispo[] = $x.$y;
				$y++;
			}
			$y = $coordY;

			//diagonale, sens : pion -> droite, bas
			$x++;
			$y++;
			while(isset($this->cases[$x.$y]) && $this->cases[$x.$y]->getJoueur() == NULL)
			{
				$caseDispo[] = $x.$y;
				$x++;
				$y++;
			}
			$x = $coordX;
			$y = $coordY;

			//axe horizontale, sens : pion -> droite
			$x++;
			while(isset($this->cases[$x.$y]) && $this->cases[$x.$y]->getJoueur() == NULL)
			{
				$caseDispo[] = $x.$y;
				$x++;
			}
			$x = $coordX;

			//diagonale, sens : pion -> droite, haut
			$x++;
			$y--;
			while(isset($this->cases[$x.$y]) && $this->cases[$x.$y]->getJoueur() == NULL)
			{
				$caseDispo[] = $x.$y;
				$x++;
				$y--;
			}
			$x = $coordX;
			$y = $coordY;

			//axe verticale, sens : pion -> haut
			$y--;
			while(isset($this->cases[$x.$y]) && $this->cases[$x.$y]->getJoueur() == NULL)
			{
				$caseDispo[] = $x.$y;
				$y--;
			}
			$y = $coordY;

			//enfin, diagonale, sens : pion -> gauche, haut
			$x--;
			$y--;
			while(isset($this->cases[$x.$y]) && $this->cases[$x.$y]->getJoueur() == NULL)
			{
				$caseDispo[] = $x.$y;
				$x--;
				$y--;
			}
			$x = $coordX;
			$y = $coordY;

			//comparaison des cases disponible et des cases disponible autour des pions à reconnecter s'il y en a
			$listeCasesReco = $this->reconnexionListeCasesDeplacement($joueur);
			if(!empty($listeCasesReco))
			{
				foreach ($caseDispo as $value) {
					if(in_array($value, $listeCasesReco))
					{
						$casesDispos[] = $value;
					}
				}
			}else{
				foreach($caseDispo as $valeur)
				{
					$casesDispos[] = $valeur;
				}
			}
			return $casesDispos;
			//on retournent la liste des cases vers lesquelles il est possible de se déplacer
		}

		/**
		@function verifPionSeul
		@param coordX int Coordonnée de colonne, coordY int Coordonnée de ligne

		@return booléen

		description
		fonction qui permet de vérifier si le pion courant n'est entouré que de cases vides (pion "null")
		*/
		function verifPionSeul($coordX, $coordY)
		{
			$x = $coordX;
			$y = $coordY;
			$pasSeul = false;


			//pour chaque case autour du pion on vérifie si elles contiennent, au moins l'une d'entre elles, qq chose d'autre qu'un pion "null"
			if($x != 1 && $this->cases[($x-1).$y]->getJoueur() != NULL){
				$pasSeul = true;
			}
			if($x != 1 && $y != 5 && $this->cases[($x-1).($y+1)]->getJoueur() != NULL){
				$pasSeul = true;
			}
			if($y != 5 && $this->cases[$x.($y+1)]->getJoueur() != NULL){
				$pasSeul = true;
			}
			if($x != 5 && $y != 5 && $this->cases[($x+1).($y+1)]->getJoueur() != NULL){
				$pasSeul = true;
			}
			if($x != 5 && $this->cases[($x+1).$y]->getJoueur() != NULL){
				$pasSeul = true;
			}
			if($x != 5 && $y != 1 && $this->cases[($x+1).($y-1)]->getJoueur() != NULL){
				$pasSeul = true;
			}
			if($y != 1 && $this->cases[$x.($y-1)]->getJoueur() != NULL){
				$pasSeul = true;
			}
			if($x != 1 && $y != 1 && $this->cases[($x-1).($y-1)]->getJoueur() != NULL){
				$pasSeul = true;
			}
			return $pasSeul;
		}

		/**
		@function deplacementImpossiblePion
		@param coordX int Coordonnée de colonne, coordY int Coordonnée de ligne, joueur Joueur Le joueur courant

		@return booléen

		description
		fonction qui permet de savoir si le pion est à côté d'un autre pion du même joueur, et donc s'il peut bouger
		*/
		function deplacementImpossiblePion($coordX, $coordY, $joueur)
		{
			$x = $coordX;
			$y = $coordY;
			$deplacementImpossible = false;

			// pour chaque case autour du pion courant, on vérifie qu'il y ai bien au moins un autre pion allié
			if(isset($this->cases[($x-1).$y]))
				if($this->cases[($x-1).$y]->getJoueur() == $joueur->getId())
				{
					$deplacementImpossible = false;
					return $deplacementImpossible;
				}else {
					$deplacementImpossible = true;
				}
			if(isset($this->cases[($x-1).($y+1)]))
				if($this->cases[($x-1).($y+1)]->getJoueur() == $joueur->getId())
				{
					$deplacementImpossible = false;
					return $deplacementImpossible;
				}else {
					$deplacementImpossible = true;
				}
			if(isset($this->cases[$x.($y+1)]))
				if($this->cases[$x.($y+1)]->getJoueur() == $joueur->getId())
				{
					$deplacementImpossible = false;
					return $deplacementImpossible;
				}else {
					$deplacementImpossible = true;
				}
			if(isset($this->cases[($x+1).($y+1)]))
				if($this->cases[($x+1).($y+1)]->getJoueur() == $joueur->getId())
				{
					$deplacementImpossible = false;
					return $deplacementImpossible;
				}else {
					$deplacementImpossible = true;
				}
			if(isset($this->cases[($x+1).$y]))
				if($this->cases[($x+1).$y]->getJoueur() == $joueur->getId())
				{
					$deplacementImpossible = false;
					return $deplacementImpossible;
				}else {
					$deplacementImpossible = true;
				}
			if(isset($this->cases[($x+1).($y-1)]))
				if($this->cases[($x+1).($y-1)]->getJoueur() == $joueur->getId())
				{
					$deplacementImpossible = false;
					return $deplacementImpossible;
				}else {
					$deplacementImpossible = true;
				}
			if(isset($this->cases[$x.($y-1)]))
				if($this->cases[$x.($y-1)]->getJoueur() == $joueur->getId())
				{
					$deplacementImpossible = false;
					return $deplacementImpossible;
				}else {
					$deplacementImpossible = true;
				}
			if(isset($this->cases[($x-1).($y-1)]))
				if($this->cases[($x-1).($y-1)]->getJoueur() == $joueur->getId())
				{
					$deplacementImpossible = false;
					return $deplacementImpossible;
				}else {
					$deplacementImpossible = true;
				}
			return $deplacementImpossible;
		}

		/**
		@function adversaireACote
		@param coordX int Coordonnée de colonne, coordY int Coordonnée de ligne, joueur Joueur Le joueur courant

		@return booléen

		description
		fonction qui permet de savoir s'il y a un pion adverse à côté du pion courant, utile pour savoir si quelqu'un a gagné
		*/
		function adversaireACote($coordX, $coordY, $joueur)
		{
			// on vérifie si les cases autour du pion courant contiennent quelque chose d'autre qu'un pion "null" mais n'étant pas allié
			if($this->deplacementImpossiblePion($coordX, $coordY, $joueur) && $this->verifPionSeul($coordX, $coordY)){
				return true;
			}else {
				return false;
			}
		}

		/**
		@function partieGagnee
		@param joueur Joueur Le joueur courant

		@return booléen

		description
		fonction qui permet de savoir si le joueur courant a gagné
		*/
		function partieGagnee($joueur)
 		{
			$gagnee = false;
			for ($y=1; $y <= 5; $y++) { // Parcour du
				for ($x=1; $x <= 5; $x++) { // plateau
					if($this->cases[$x.$y]->getJoueur() == $joueur->getId()) // si la case contient un pion appartenant au joueur courant
					{
						// et s'il est à côté d'un adversaire sans qu'il puisse se déplacer (voir fonction adversaireACote)
						if($this->adversaireACote($x, $y, $joueur))
			 			{
			 				$gagnee = true;
			 			}
						else
						{
							$gagnee = false;
							return $gagnee;
						}
					}
				}
		 	}
			return $gagnee;
		}

		/**
		@function reconnexion
		@param coordX int Coordonnée de colonne, coordY int Coordonnée de ligne

		@return Pion

		description
		fonction qui permet de savoir si le pion courant doit être reconnecté
		*/
		function reconnexion($coordX, $coordY)
		{
			if($this->verifPionSeul($coordX, $coordY) == false) // si le pion est seul
			{
				return $this->cases[$coordX.$coordY]; // on le retournent
			}
			else
			{
				return NULL; // sinon on retournent NULL
			}
		}

		/**
		@function reconnexionListePion
		@param joueur Joueur Le joueur courant

		@return Pion[]

		description
		fonction qui permet de retourner la liste des pions du joueur courant qui doivent être reconnectés
		*/
		function reconnexionListePion($joueur)
		{
			$pionsAReconnecter = array();

			for ($y=1; $y <= 5; $y++) { // Parcour du
				for ($x=1; $x <= 5; $x++) { // plateau
					if($this->cases[$x.$y]->getJoueur() == $joueur->getId()) // si le pion appartient au joueur courant
		 			{
		 				$pionsAReconnecter[] = $this->reconnexion($x, $y); // on vérifie s'il doit être reconnecté
		 			}
				}
		 	}
			return $pionsAReconnecter; // on renvoit la liste de pion à reconnecter
		}

		/**
		@function reconnexionListeCasesDeplacement
		@param joueur Joueur Le joueur courant

		@return int[] (tableau de coordonnée)

		description
		fonction qui permet de retourner la liste des déplacements possible dans le cas où il y a des pions à reconnecter
		*/
		function reconnexionListeCasesDeplacement($joueur)
		{
			$pionsAReconnecter = $this->reconnexionListePion($joueur); // on récupèrent la liste de pion à reconnecter
			$casesDispo = array();

			// pour chaque élément de cette liste de pion, on récupère les coordonnées autour d'eux
			foreach ($pionsAReconnecter as $key => $value)
			{
				if($value != NULL){
					$x = $value->getX();
					$y = $value->getY();

					if($x != 1 && $this->cases[($x-1).$y]->getJoueur() == NULL){
						$casesDispo[] = ($x-1).$y;
					}
					if($x != 1 && $y != 5 && $this->cases[($x-1).($y+1)]->getJoueur() == NULL){
						$casesDispo[] = ($x-1).($y+1);
					}
					if($y != 5 && $this->cases[$x.($y+1)]->getJoueur() == NULL){
						$casesDispo[] = $x.($y+1);
					}
					if($x != 5 && $y != 5 && $this->cases[($x+1).($y+1)]->getJoueur() == NULL){
						$casesDispo[] = ($x+1).($y+1);
					}
					if($x != 5 && $this->cases[($x+1).$y]->getJoueur() == NULL){
						$casesDispo[] = ($x+1).$y;
					}
					if($x != 5 && $y != 1 && $this->cases[($x+1).($y-1)]->getJoueur() == NULL){
						$casesDispo[] = ($x+1).($y-1);
					}
					if($y != 1 && $this->cases[$x.($y-1)]->getJoueur() == NULL){
						$casesDispo[] = $x.($y-1);
					}
					if($x != 1 && $y != 1 && $this->cases[($x-1).($y-1)]->getJoueur() == NULL){
						$casesDispo[] = ($x-1).($y-1);
					}
				}
			}
			return $casesDispo;
		}

		/**
		@function deplacementPion
		@param coordXAvantDep int Coordonnée de colonne, coordYAvantDep int Coordonnée de ligne
		@param coordXApresDep int Coordonnée de colonne, coordYApresDep int Coordonnée de ligne
		@param joueur Joueur Le joueur courant

		@return booléen

		description
		fonction qui permet d'échanger la place entre un pion du joueur courant et une case vide (pion "null")
		*/
		function deplacementPion($coordXAvantDep, $coordYAvantDep, $coordXApresDep, $coordYApresDep, $joueur)
		{
			if($this->deplacementImpossiblePion($coordXAvantDep ,$coordYAvantDep, $joueur) == false)
			{
				// on échangent le pion que l'on veut déplacer avec le pion "null" se trouvant à l'emplacement visé et on redéfinit leur coordonnées
				$tmpPion = $this->cases[$coordXApresDep.$coordYApresDep];

				$this->cases[$coordXApresDep.$coordYApresDep] = $this->cases[$coordXAvantDep.$coordYAvantDep];
				$this->cases[$coordXAvantDep.$coordYAvantDep]->setX($coordXAvantDep);
				$this->cases[$coordXAvantDep.$coordYAvantDep]->setY($coordYAvantDep);

				$this->cases[$coordXAvantDep.$coordYAvantDep] = $tmpPion;
				$this->cases[$coordXApresDep.$coordYApresDep]->setX($coordXApresDep);
				$this->cases[$coordXApresDep.$coordYApresDep]->setY($coordYApresDep);
				return true; // on retournent true quand l'opération s'est bien déroulée
			}else {
				return false; // sinon false
			}
		}

		/**
		@function passeTour
		@param joueur Joueur Le joueur courant

		@return booléen

		description
		fonction qui permet de savoir si le tour du joueur courant doit être passé, en fonction des déplacements disponible (aucun : le tour est passé)
		*/
		function passeTour($joueur)
		{
			$passeSonTour = false;
			for ($y=1; $y <= 5; $y++) { // Parcour du
				for ($x=1; $x <= 5; $x++) { // plateau
					// si aucun des pions, appartenant au joueur courant, ne peuvent se déplacer alors il doit passer son tour
					if($this->cases[$x.$y]->getJoueur() == $joueur->getId() && $this->reconnexion($x, $y) == NULL)
					{
						if(($this->deplacementPossiblePion($x, $y, $joueur) == NULL))
			 			{
			 				$passeSonTour = true;
			 			}
						else
						{
							$passeSonTour = false;
							return $passeSonTour;
						}
					}
				}
		 	}
			return $passeSonTour;
		}

		function __toString()
		{
			$s = '';
			for ($y=1; $y <= 5; $y++)
			{
				for ($x=1; $x <= 5; $x++)
				{
					$s = $s . var_dump($x.','.$y).var_dump($this->cases[$x.$y]);
				}
			}
			return $s;
		}
	}
